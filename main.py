import sys

DELIMITER = "."

class Process:
    def __init__(self, name, arrival_time, execute_time, waiting_time = 0):
        self.name = name
        self.arrival_time = arrival_time
        self.execute_time = execute_time
        self.waiting_time = waiting_time

def round_robin(processes, time_slice):
    time = 0
    processes.sort(key=lambda x: x.arrival_time)
    
    while len(processes) > 0:
        for process in processes:
            time += time_slice

            if process.execute_time >= time_slice:
                process.execute_time -= time_slice
            else:
                process.execute_time = 0

            if process.execute_time == 0:
                process.waiting_time = time
                ready_queue.append(process)

            print((DELIMITER * time) + process.name)

        processesHasNotFinished = []
        for process in processes:
            if process.execute_time > 0:
                processesHasNotFinished.append(process)
        
        processes = processesHasNotFinished


def print_waiting_time(ready_queue):
    print("\n") 
    total_waiting = 0

    for process in ready_queue:
        total_waiting += process.waiting_time
        print(process.name + " waiting time: " + str(process.waiting_time))

    print("\nAverage waiting time: " + str(round(total_waiting / len(ready_queue), 2)))

print("Digite um time slice: ")

processes = [
    Process("P1", 0, 10),
    Process("P2", 1, 5),
    Process("P3", 2, 15)
]
time_slice = int(sys.stdin.readline())
ready_queue = []

round_robin(processes, time_slice)
print_waiting_time(ready_queue)