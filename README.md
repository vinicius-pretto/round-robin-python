# [Round Robin](https://en.wikipedia.org/wiki/Round-robin_scheduling)

Este algoritmo é usado primordialmente no compartilhamento de tempo e no [sistema multiusuário](https://pt.wikipedia.org/wiki/Multiusu%C3%A1rio).  
Onde os principais requisitos são o tempo de resposta. Nesse algoritmo o tempo da CPU é dividido em [time slices](<https://en.wikipedia.org/wiki/Preemption_(computing)#Time_slice>) (fatias de tempo).  
Cada processo é alocado um **time slice** enquanto está sendo executado.  
Nenhum processo pode ser executado por mais de um **time slice** enquanto houver outros processos esperando na **ready queue** (fila pronta).  
Se o processo exigir um tempo maior que o **time slice**, então deve ir no final da **ready queue** (fila pronta).

Exemplo:

Time slice = 5

| Process name | Arrival time | Execute time |
| ------------ | ------------ | ------------ |
| P1           | 0            | 10           |
| P2           | 1            | 5            |
| P3           | 2            | 15           |

Baseado na tabela acima o resultado seria:

```
.....P1
..........P2
...............P3
....................P1
.........................P3
..............................P3


P2 waiting time: 10
P1 waiting time: 20
P3 waiting time: 30

Average waiting time: 20.0
```
